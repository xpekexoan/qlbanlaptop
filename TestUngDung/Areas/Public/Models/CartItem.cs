﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestUngDung.Areas.Public.Models
{
    [Serializable]
    public class CartItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double UnitCost { get; set; }
        public int Quantity { get; set; }
        public double intoMoney
        {
            get { return UnitCost * Quantity; }
        }
    }
}