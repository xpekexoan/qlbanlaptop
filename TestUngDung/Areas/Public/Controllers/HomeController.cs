﻿using ModelEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace TestUngDung.Areas.Public.Controllers
{
    public class HomeController : Controller
    {
        NguyenVanTriContext db = new NguyenVanTriContext();

        public ActionResult Index()
        {
            var products = db.Products.Include(s => s.Category);
            return View(products.ToList());
        }
    }
}