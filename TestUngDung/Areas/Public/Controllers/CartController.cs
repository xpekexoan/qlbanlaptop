﻿using ModelEF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using TestUngDung.Areas.Public.Models;

namespace TestUngDung.Areas.Public.Controllers
{
    public class CartController : Controller
    {
        private NguyenVanTriContext db = new NguyenVanTriContext();
        public ActionResult Index()
        {
            List<CartItem> giohang = Session["giohang"] as List<CartItem>;
            return View(giohang);
        }
        // Khai báo phương thức thêm sản phẩm vào giỏ hàng
        public RedirectToRouteResult AddToCart(string MaSP)
        {
            if (Session["giohang"] == null)
            {
                Session["giohang"] = new List<CartItem>();
            }
            List<CartItem> giohang = Session["giohang"] as List<CartItem>;
            // Kiểm tra sản phẩm khách đang chọn có trong giỏ hàng hay chưa
            if (giohang.FirstOrDefault(m => m.Id == MaSP) == null)
            {
                Product product = db.Products.Find(MaSP);
                CartItem newItem = new CartItem();
                newItem.Id = MaSP;
                newItem.Name = product.Name;
                newItem.Quantity = 1;
                newItem.UnitCost = Convert.ToDouble(product.UnitCost);
                giohang.Add(newItem);

            }
            else // Nếu sản phẩm đã có trong giỏ hàng ==> tăng số lượng lên 1
            {
                CartItem cartItem = giohang.FirstOrDefault(m => m.Id == MaSP);
                cartItem.Quantity++;
            }
            Session["giohang"] = giohang;
            return RedirectToAction("Index");
        }
        public RedirectToRouteResult Update(string MaSP, int txtSoLuong)
        {
            List<CartItem> giohang = Session["giohang"] as List<CartItem>;
            CartItem item = giohang.FirstOrDefault(m => m.Id == MaSP);
            if (item != null)
            {
                item.Quantity = txtSoLuong;
                Session["giohang"] = giohang;
            }
            return RedirectToAction("Index");

        }
        public RedirectToRouteResult DelCartItem(string MaSP)
        {
            List<CartItem> giohang = Session["giohang"] as List<CartItem>;
            CartItem item = giohang.FirstOrDefault(m => m.Id == MaSP);
            if (item != null)
            {
                giohang.Remove(item);
                Session["giohang"] = giohang;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Order(string Email, string Phone)
        {
            List<CartItem> giohang = Session["giohang"] as List<CartItem>;
            string sMsg = "<html><body><table border='1'> <caption> Thông tin đặt hàng </caption> <tr><th>STT </th><th>Tên sản phẩm</th> <th>Số lượng</th> <th> Đơn giá</th><th>Thành tiền</th></tr>";
            int i = 0;
            double tongtien = 0;
            foreach (CartItem item in giohang)
            {
                i++;
                sMsg += "<tr>";
                sMsg += "<td>" + i.ToString() + "</td>";
                sMsg += "<td>" + item.Name + "</td>";
                sMsg += "<td>" + item.Quantity.ToString() + "</td>";
                sMsg += "<td>" + item.UnitCost.ToString() + "</td>";
                sMsg += "<td>" + String.Format("{0:#,###}", item.Quantity * item.UnitCost) + "</td>";
                sMsg += "</tr>";
                tongtien += item.Quantity * item.UnitCost;

            }
            sMsg += "<tr><th colspan='5'>Tổng cộng: " +
                String.Format("{0:#,### vnđ}", tongtien) + "</th></tr></table>";
            MailMessage mail = new MailMessage("yourshape1989@gmail.com", Email, "Thông tin đơn hàng", sMsg);
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.Credentials = new NetworkCredential("yourshape1989", "muathutroinhanh142857");
            mail.IsBodyHtml = true;
            client.Send(mail);
            return RedirectToAction("Index", "Home");

        }
    }
}