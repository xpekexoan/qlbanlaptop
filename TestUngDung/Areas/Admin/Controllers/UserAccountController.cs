﻿using ModelEF.DAO;
using System.Web.Mvc;

namespace TestUngDung.Areas.Admin.Controllers
{
    public class UserAccountController : Controller
    {
        // GET: Admin/UserAccount
        public ActionResult Index(string searchString, int page = 1, int pageSize = 5)
        {
            var session = (TestUngDung.Areas.Admin.Models.LoginModel)Session[TestUngDung.Common.Contrant.USER_SESSION];
            if (session == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var user = new UserAccountDAO();
            var model = user.ListWhereAll(searchString,page, pageSize);
            ViewBag.SearchString = searchString;
            return View(model);
        }
            
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            new UserAccountDAO().Delete(id);

            return RedirectToAction("Index");
        }
    }
}