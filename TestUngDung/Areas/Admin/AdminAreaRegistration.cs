﻿using System.Web.Mvc;

namespace TestUngDung.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Delete_default",
                "Admin/{controller}/{action}/{id}",
                new { controller = "UserAccount",action = "Delete", id = UrlParameter.Optional }
            );

            //context.MapRoute(
            //    "UserAccount_Index_default",
            //    "Admin/{controller}/{action}/{id}",
            //    new { controller = "UserAccount", action = "Index", id = UrlParameter.Optional }
            //);

            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}