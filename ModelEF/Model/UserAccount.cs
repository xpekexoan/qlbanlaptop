﻿namespace ModelEF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserAccount")]
    public partial class UserAccount
    {
        public int Id { get; set; }

        [StringLength(50)]
        [DisplayName("Tên đăng nhập")]
        public string UserName { get; set; }
        [DisplayName("Mậu khẩu")]
        public string Passwords { get; set; }
        [DisplayName("Trạng thái")]
        public int? Status { get; set; }
    }
    
}
