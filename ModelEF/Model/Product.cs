﻿namespace ModelEF.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        public int Id { get; set; }

        [StringLength(100)]
        [DisplayName("Tên sản phẩm")]
        [Required(ErrorMessage ="Bạn chưa nhập tên sản phẩm")]
        public string Name { get; set; }

        [DisplayName("Đơn giá")]
        [Required(ErrorMessage = "Bạn chưa nhập đơn giá")]
        public double? UnitCost { get; set; }

        [DisplayName("Số lượng")]
        [Required(ErrorMessage = "Bạn chưa nhập số lượng")]
        public int? Quantity { get; set; }

        [DisplayName("Hình sản phẩm")]
        public string Image { get; set; }

        [DisplayName("Mô tả")]
        [Required(ErrorMessage = "Bạn chưa nhập mô tả")]
        public string Description { get; set; }
        [Range(0,1, ErrorMessage ="Bạn phải nhập số 0 hoặc 1")]
        public int? Status { get; set; }
        [DisplayName("Danh mục")]
        public int? CategoryId { get; set; }
        [DisplayName("Danh mục")]
        public virtual Category Category { get; set; }
    }
}
